package com.company;

public class Main {

    public static void main(String[] args) {
        Vehicle vehicle1 = new CarVehicle();
        vehicle1.setBrand("BMW");
        vehicle1.setModel("M2");
        vehicle1.setProductionYear(2016);
        vehicle1.writeTo(new FileWriter());
        vehicle1.printInfo();

        Vehicle vehicle2 = new MotorVehicle();
        vehicle2.setBrand("Yamaha");
        vehicle2.setModel("R6");
        vehicle2.setProductionYear(2008);
        vehicle2.writeTo(new GoogleDriveWriter());
        vehicle2.printInfo();
    }
}
