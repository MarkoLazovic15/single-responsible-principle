package com.company;

public interface IWriter {
    void saveTo(String filename);
}
