package com.company;

public class CarVehicle extends Vehicle {
    @Override
    void printInfo() {
        System.out.println("Type: Car");
        System.out.println("Brand: " + getBrand());
        System.out.println("Model: " + getModel());
        System.out.println("Production Year:" + getProductionYear());
    }
}
