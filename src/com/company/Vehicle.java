package com.company;

public abstract class Vehicle {
    private String brand;
    private String model;
    private int productionYear;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }
    public void writeTo(IWriter writer){
        writer.saveTo("data");
    }
    abstract void printInfo();
}
