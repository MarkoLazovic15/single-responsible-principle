package com.company;

public class MotorVehicle extends Vehicle {
    @Override
    void printInfo() {
        System.out.println("Type: Motor");
        System.out.println("Brand: " + getBrand());
        System.out.println("Model: " + getModel());
        System.out.println("Production Year:" + getProductionYear());
    }
}
